-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: HOSPITAL
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Checkup`
--

DROP TABLE IF EXISTS `Checkup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Checkup` (
  `patient_id` int NOT NULL,
  `doctor_id` int NOT NULL,
  `cause` text NOT NULL,
  `datetime` timestamp NOT NULL,
  `measures` text NOT NULL,
  PRIMARY KEY (`patient_id`,`datetime`),
  KEY `patient_id` (`patient_id`),
  KEY `datetime` (`datetime`),
  KEY `Checkup_ibfk_2` (`doctor_id`),
  CONSTRAINT `Checkup_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Patient` (`patient_id`),
  CONSTRAINT `Checkup_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `Doctor` (`doctor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Checkup`
--

LOCK TABLES `Checkup` WRITE;
/*!40000 ALTER TABLE `Checkup` DISABLE KEYS */;
INSERT INTO `Checkup` VALUES (1,8,'joint pains','2019-09-18 10:00:00','massage with hot oil'),(2,1,'fever','2019-09-18 10:00:00','increase the intake quantity of fruits'),(3,5,'throat infection','2019-09-18 10:00:00','replace normal drinking water eith luke warm water.'),(4,4,'cold','2019-09-18 11:05:00','replace normal drinking water eith luke warm water.'),(5,5,'breathing problems','2019-09-13 11:05:00','steam inhalation');
/*!40000 ALTER TABLE `Checkup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Department`
--

DROP TABLE IF EXISTS `Department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Department` (
  `deptname` varchar(20) NOT NULL,
  `head_id` int NOT NULL,
  PRIMARY KEY (`deptname`),
  KEY `Department_ibfk_1` (`head_id`),
  CONSTRAINT `Department_ibfk_1` FOREIGN KEY (`head_id`) REFERENCES `Employee` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Department`
--

LOCK TABLES `Department` WRITE;
/*!40000 ALTER TABLE `Department` DISABLE KEYS */;
INSERT INTO `Department` VALUES ('Pediatrics',1),('Cardiology',2),('Neurology',5),('Oncology',8),('Anesthesiology',10);
/*!40000 ALTER TABLE `Department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Details`
--

DROP TABLE IF EXISTS `Details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Details` (
  `Description` text NOT NULL,
  `street` varchar(30) NOT NULL,
  `pincode` int DEFAULT NULL,
  `state` varchar(10) NOT NULL,
  `city` varchar(10) NOT NULL,
  `timings` varchar(20) NOT NULL,
  `contact_number` int NOT NULL,
  `email_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Details`
--

LOCK TABLES `Details` WRITE;
/*!40000 ALTER TABLE `Details` DISABLE KEYS */;
INSERT INTO `Details` VALUES ('This is a multi speciality hospital.','anaconda street',68107,'Nebraska','Ohama','24/7',123456789,'hospital@hospital');
/*!40000 ALTER TABLE `Details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Doctor`
--

DROP TABLE IF EXISTS `Doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Doctor` (
  `doctor_id` int NOT NULL,
  `trainer_id` int DEFAULT NULL,
  `email_id` varchar(20) NOT NULL,
  PRIMARY KEY (`doctor_id`),
  KEY `Doctor_ibfk_2` (`trainer_id`),
  CONSTRAINT `Doctor_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `Employee` (`employee_id`),
  CONSTRAINT `Doctor_ibfk_2` FOREIGN KEY (`trainer_id`) REFERENCES `Employee` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Doctor`
--

LOCK TABLES `Doctor` WRITE;
/*!40000 ALTER TABLE `Doctor` DISABLE KEYS */;
INSERT INTO `Doctor` VALUES (1,2,'Daisy@gary'),(2,NULL,'Wilson@Thompson'),(3,NULL,'Andrew@Holmes'),(4,2,'Charlotte@Hamilton'),(5,2,'James@Grant'),(6,3,'Chester@West'),(7,3,'Jack@Stewart'),(8,2,'Edith@Fowler'),(9,NULL,'cool@dude');
/*!40000 ALTER TABLE `Doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Doctor_Medical_Background`
--

DROP TABLE IF EXISTS `Doctor_Medical_Background`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Doctor_Medical_Background` (
  `doctor_id` int NOT NULL,
  `practice_info` text NOT NULL,
  `degree` text NOT NULL,
  `education_background` text NOT NULL,
  PRIMARY KEY (`doctor_id`),
  CONSTRAINT `Doctor_Medical_Background_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `Doctor` (`doctor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Doctor_Medical_Background`
--

LOCK TABLES `Doctor_Medical_Background` WRITE;
/*!40000 ALTER TABLE `Doctor_Medical_Background` DISABLE KEYS */;
INSERT INTO `Doctor_Medical_Background` VALUES (1,'1 year after MBBS and 3 years afetr MS/MD.','MBBS','After(10+2),MBBS(5 year)undergrad and pediatrics of MD/MS(3 year)post grad.'),(2,'1 year after MBBS and 3 years afetr MS/MD.','MBBS','After(10+2),MBBS(5 year)undergrad and Cardiology of MD/MS(3 year)post grad.'),(3,'1 year after MBBS and 3 years afetr MS/MD.','MBBS','After(10+2),MBBS(5 year)undergrad and Cardiology of MD/MS(3 year)post grad.'),(4,'1 year after MBBS and 3 years afetr MS/MD.','MBBS','After(10+2),MBBS(5 year)undergrad and Pediatrics of MD/MS(3 year)post grad.'),(5,'1 year after MBBS and 3 years afetr MS/MD.','MBBS','After(10+2),MBBS(5 year)undergrad and Neurology of MD/MS(3 year)post grad.'),(6,'1 year after MBBS and 3 years afetr MS/MD.','MBBS','After(10+2),MBBS(5 year)undergrad and Oncology of MD/MS(3 year)post grad.'),(7,'1 year after MBBS and 3 years afetr MS/MD.','MBBS','After(10+2),MBBS(5 year)undergrad and Neurology of MD/MS(3 year)post grad.'),(8,'1 year after MBBS and 3 years afetr MS/MD.','MBBS','After(10+2),MBBS(5 year)undergrad and Oncology of MD/MS(3 year)post grad.'),(9,'1 year after MBBS and 3 years afetr MS/MD.','MBBS','After(10+2),MBBS(5 year)undergrad and Neurology of MD/MS(3 year)post grad.');
/*!40000 ALTER TABLE `Doctor_Medical_Background` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Druggist`
--

DROP TABLE IF EXISTS `Druggist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Druggist` (
  `druggist_id` int NOT NULL,
  `working_shop_id` int NOT NULL,
  `email_id` varchar(20) NOT NULL,
  PRIMARY KEY (`druggist_id`),
  KEY `Druggist_ibfk_2` (`working_shop_id`),
  CONSTRAINT `Druggist_ibfk_1` FOREIGN KEY (`druggist_id`) REFERENCES `Employee` (`employee_id`),
  CONSTRAINT `Druggist_ibfk_2` FOREIGN KEY (`working_shop_id`) REFERENCES `pharmacy` (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Druggist`
--

LOCK TABLES `Druggist` WRITE;
/*!40000 ALTER TABLE `Druggist` DISABLE KEYS */;
INSERT INTO `Druggist` VALUES (15,1,'Frederick@Kelly'),(16,2,'Tara@Kelly'),(17,3,'Edwin@Scott');
/*!40000 ALTER TABLE `Druggist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Druggist_Medical_Background`
--

DROP TABLE IF EXISTS `Druggist_Medical_Background`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Druggist_Medical_Background` (
  `Druggist_id` int NOT NULL,
  `practice_info` text NOT NULL,
  `degree` text NOT NULL,
  `education_background` text NOT NULL,
  PRIMARY KEY (`Druggist_id`),
  CONSTRAINT `Druggist_Medical_Background_ibfk_1` FOREIGN KEY (`Druggist_id`) REFERENCES `Druggist` (`druggist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Druggist_Medical_Background`
--

LOCK TABLES `Druggist_Medical_Background` WRITE;
/*!40000 ALTER TABLE `Druggist_Medical_Background` DISABLE KEYS */;
INSERT INTO `Druggist_Medical_Background` VALUES (15,'1 year after PharmD','PharmD','After(10+2),did pharmD in IIP'),(16,'1 year after PharmD','PharmD','After(10+2),did pharmD in IIP'),(17,'1 year after PharmD','PharmD','After(10+2),did pharmD in IIP');
/*!40000 ALTER TABLE `Druggist_Medical_Background` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Employee`
--

DROP TABLE IF EXISTS `Employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Employee` (
  `employee_id` int NOT NULL AUTO_INCREMENT,
  `Fname` varchar(15) NOT NULL,
  `Lname` varchar(15) NOT NULL,
  `country_code` int NOT NULL,
  `phone_number` int NOT NULL,
  `salary` decimal(10,2) DEFAULT NULL,
  `street` varchar(30) DEFAULT NULL,
  `pincode` int DEFAULT NULL,
  `job_type` varchar(10) NOT NULL,
  `present_status` varchar(50) DEFAULT NULL,
  `DOB` date NOT NULL,
  `sex` char(1) NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `Employee_ibfk_2` (`pincode`),
  CONSTRAINT `Employee_ibfk_1` FOREIGN KEY (`pincode`) REFERENCES `city_codes` (`pincode`),
  CONSTRAINT `Employee_ibfk_2` FOREIGN KEY (`pincode`) REFERENCES `state_codes` (`pincode`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Employee`
--

LOCK TABLES `Employee` WRITE;
/*!40000 ALTER TABLE `Employee` DISABLE KEYS */;
INSERT INTO `Employee` VALUES (1,'Daisy','Gray',791,739942238,55000.00,'9653 South Woodsman St',6611,'Doctor','Currently working in this hospital.','1984-12-02','F'),(2,'Wilson','Thompson',791,460934200,75000.00,'23 Harrison Dr',29150,'Doctor','Currently working in this hospital.','1984-12-12','M'),(3,'Andrew','Holmes',791,586495412,60000.00,'753 Circle Avenue ',12804,'Doctor','Currently working in this hospital.','1986-02-02','M'),(4,'Charlotte','Hamilton',791,437511025,50000.00,'23 Forest Street ',44512,'Doctor','Currently working in this hospital.','1987-05-02','F'),(5,'James','Grant',791,698216154,60000.00,'825 Myrtle Ave',27529,'Doctor','Currently working in this hospital.','1983-02-12','M'),(6,'Chester','West',791,562414567,57000.00,'8 Swanson Circle ',46342,'Doctor','Currently working in this hospital.','1985-02-01','M'),(7,'Jack','Stewart',791,155159395,59000.00,'86 Foxrun Road ',20901,'Doctor','Currently working in this hospital.','1988-06-02','M'),(8,'Chester','Stevens',791,458602273,60000.00,'245 Lake View St',47711,'Doctor','Currently working in this hospital.','1988-11-11','M'),(9,'Edith','Fowler',792,459034188,NULL,'8146 Jennings Ave.',46140,'Doctor','Retired.','1985-01-10','M'),(10,'Jessica','Holmes',791,968895384,35000.00,'7797 Randall Mill Drive',95820,'Nurse','Currently working in this hospital.','1990-02-10','F'),(11,'Olivia','Wells',791,390966587,30000.00,'68 Fremont Street',77904,'Nurse','Currently working in this hospital.','1990-02-11','F'),(12,'Honey','Johnson',791,477133568,27000.00,'10 Warren Rd.',1420,'Nurse','Currently working in this hospital.','1991-02-01','F'),(13,'Brianna','Dixon',791,349228564,25000.00,'63 Euclid Ave.',80003,'Nurse','Currently working in this hospital.','1992-02-01','F'),(14,'Miranda','Anderson',791,479737487,25000.00,'27 Forest Lane',28540,'Nurse','Currently working in this hospital.','1992-12-11','F'),(15,'Frederick','Kelly',791,339607268,20000.00,'9879 Lower River Avenue',29841,'Druggist','Currently working in this hospital.','1986-02-01','M'),(16,'Tara','Kelly',791,641577571,20000.00,'9879 Lower River Avenue',29841,'Druggist','Currently working in this hospital.','1986-02-09','F'),(17,'Edwin','Scott',791,472563016,22000.00,'8163 Alton Dr',11050,'Druggist','Currently working in this hospital.','1986-12-12','M'),(18,'Jimmy','Anderson',791,472336541,12000.00,'7235 Atlin street',28540,'Janitor','Currently working in this hospital.','1980-12-12','M'),(19,'Sarfraz','Khan',791,472336896,10000.00,'7235 bakers street',29841,'Janitor','Currently working in this hospital.','1980-05-05','M'),(20,'Josh','Ahmed',791,473536896,10000.00,'6213 Adams street',80003,'Janitor','Currently working in this hospital.','1980-07-05','M'),(21,'Adam','Smith',791,473536897,10000.00,'6213 coners street ',80003,'Janitor','Currently working in this hospital.','1980-02-08','M');
/*!40000 ALTER TABLE `Employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Nurse`
--

DROP TABLE IF EXISTS `Nurse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Nurse` (
  `nurse_id` int NOT NULL,
  `trainer_id` int DEFAULT NULL,
  `email_id` varchar(20) NOT NULL,
  PRIMARY KEY (`nurse_id`),
  KEY `Nurse_ibfk_2` (`trainer_id`),
  CONSTRAINT `Nurse_ibfk_1` FOREIGN KEY (`nurse_id`) REFERENCES `Employee` (`employee_id`),
  CONSTRAINT `Nurse_ibfk_2` FOREIGN KEY (`trainer_id`) REFERENCES `Employee` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Nurse`
--

LOCK TABLES `Nurse` WRITE;
/*!40000 ALTER TABLE `Nurse` DISABLE KEYS */;
INSERT INTO `Nurse` VALUES (10,NULL,'Jessica@Holmes'),(11,10,'Olivia@Wells'),(12,10,'Honey@Johnson'),(13,10,'Brianna@Dixon'),(14,10,'Miranda@Anderson');
/*!40000 ALTER TABLE `Nurse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Nurse_Medical_Background`
--

DROP TABLE IF EXISTS `Nurse_Medical_Background`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Nurse_Medical_Background` (
  `nurse_id` int NOT NULL,
  `practice_info` text NOT NULL,
  `degree` text NOT NULL,
  `education_background` text NOT NULL,
  PRIMARY KEY (`nurse_id`),
  CONSTRAINT `Nurse_Medical_Background_ibfk_1` FOREIGN KEY (`nurse_id`) REFERENCES `Nurse` (`nurse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Nurse_Medical_Background`
--

LOCK TABLES `Nurse_Medical_Background` WRITE;
/*!40000 ALTER TABLE `Nurse_Medical_Background` DISABLE KEYS */;
INSERT INTO `Nurse_Medical_Background` VALUES (10,'1 year after getting BSN.','BSN','After(10+2),Bachelor of science.'),(11,'1 year after getting BSN.','BSN','After(10+2),Bachelor of science.'),(12,'1 year after getting BSN.','BSN','After(10+2),Bachelor of science.'),(13,'1 year after getting BSN.','BSN','After(10+2),Bachelor of science.'),(14,'1 year after getting BSN.','BSN','After(10+2),Bachelor of science.');
/*!40000 ALTER TABLE `Nurse_Medical_Background` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Operation`
--

DROP TABLE IF EXISTS `Operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Operation` (
  `patient_id` int NOT NULL,
  `doctor_id` int NOT NULL,
  `Nurse_id` int NOT NULL,
  `Room_id` int NOT NULL,
  `medical_condition` text NOT NULL,
  `admitted_time` timestamp NOT NULL,
  `discharge_time` timestamp NOT NULL,
  PRIMARY KEY (`patient_id`,`admitted_time`),
  KEY `patient_id` (`patient_id`),
  KEY `admitted_time` (`admitted_time`),
  KEY `Operation_ibfk_2` (`doctor_id`),
  KEY `Operation_ibfk_3` (`Nurse_id`),
  KEY `Operation_ibfk_4` (`Room_id`),
  CONSTRAINT `Operation_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Patient` (`patient_id`),
  CONSTRAINT `Operation_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `Doctor` (`doctor_id`),
  CONSTRAINT `Operation_ibfk_3` FOREIGN KEY (`Nurse_id`) REFERENCES `Nurse` (`nurse_id`),
  CONSTRAINT `Operation_ibfk_4` FOREIGN KEY (`Room_id`) REFERENCES `Room` (`Room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Operation`
--

LOCK TABLES `Operation` WRITE;
/*!40000 ALTER TABLE `Operation` DISABLE KEYS */;
INSERT INTO `Operation` VALUES (6,3,10,7,'heart surgery','2019-09-17 10:00:00','2019-10-17 10:00:00'),(7,6,10,6,'knee surgery','2019-09-17 10:00:00','2019-10-17 10:00:00'),(8,5,12,4,'Appendectomy','2019-09-17 10:00:00','2019-10-17 10:00:00');
/*!40000 ALTER TABLE `Operation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Patient`
--

DROP TABLE IF EXISTS `Patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Patient` (
  `patient_id` int NOT NULL AUTO_INCREMENT,
  `Fname` varchar(15) NOT NULL,
  `Lname` varchar(15) NOT NULL,
  `country_code` int NOT NULL,
  `phone_number` int NOT NULL,
  `DOB` date DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `street` varchar(30) DEFAULT NULL,
  `pincode` int DEFAULT NULL,
  PRIMARY KEY (`patient_id`),
  KEY `Patient_ibfk_2` (`pincode`),
  CONSTRAINT `Patient_ibfk_1` FOREIGN KEY (`pincode`) REFERENCES `city_codes` (`pincode`),
  CONSTRAINT `Patient_ibfk_2` FOREIGN KEY (`pincode`) REFERENCES `state_codes` (`pincode`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Patient`
--

LOCK TABLES `Patient` WRITE;
/*!40000 ALTER TABLE `Patient` DISABLE KEYS */;
INSERT INTO `Patient` VALUES (1,'Haroid','Inghan',791,189474679,'1989-11-11','M','9797 Thatcher St',33414),(2,'Joyce','Hamilton',788,273581938,'1992-08-19','F','315 Lawrence St',95677),(3,'Albert','Gibson',621,437961187,'1995-01-12','M','20 Marshall Drive',14701),(4,'Olivia','Davis',791,778176381,'1993-02-11','F','7843 Maiden Court',68107),(5,'Kirsten','Scott',791,746993021,'1992-02-13','F','95 Tanglewood Ave.',23703),(6,'Olivia','Martin',693,848603063,'1977-07-16','F','205 Essex Lane',36605),(7,'Maddie','Harrison',693,848603064,'1970-08-16','M','205 Essex Lane',36605),(8,'Henry','Carter',643,416187796,'1972-08-13','M','60 Woodside Drive ',28803);
/*!40000 ALTER TABLE `Patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Room`
--

DROP TABLE IF EXISTS `Room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Room` (
  `Room_id` int NOT NULL AUTO_INCREMENT,
  `room_type` varchar(20) NOT NULL,
  `booked` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Room`
--

LOCK TABLES `Room` WRITE;
/*!40000 ALTER TABLE `Room` DISABLE KEYS */;
INSERT INTO `Room` VALUES (1,'Sharing Ward',0),(2,'Sharing Ward',0),(3,'Private Room',0),(4,'Private Room',0),(5,'Private Room',0),(6,'Deluxe Room',0),(7,'Deluxe Room',0);
/*!40000 ALTER TABLE `Room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_codes`
--

DROP TABLE IF EXISTS `city_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `city_codes` (
  `pincode` int NOT NULL,
  `city` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`pincode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_codes`
--

LOCK TABLES `city_codes` WRITE;
/*!40000 ALTER TABLE `city_codes` DISABLE KEYS */;
INSERT INTO `city_codes` VALUES (1420,'Fitchburg'),(6611,'Trumbull'),(11050,'Port Washington'),(12804,'Queensbury'),(14701,'Jamestown'),(20901,'Silver Spring'),(23703,'Bridgewater'),(27529,'Garner'),(28540,'Jacksonville'),(28803,'Asheville'),(29150,'Sumter'),(29841,'North Augusta'),(33414,'Wellington'),(36605,'Mobile'),(44512,'Youngstown'),(46140,'Greenfield'),(46342,'Hobart'),(47711,'Evansville'),(68107,'Omaha'),(77904,'Victoria'),(80003,'Arvada'),(95677,'Rocklin'),(95820,'Sacramento');
/*!40000 ALTER TABLE `city_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contains`
--

DROP TABLE IF EXISTS `contains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contains` (
  `med_code` int NOT NULL,
  `phar_id` int NOT NULL,
  PRIMARY KEY (`med_code`,`phar_id`),
  KEY `contains_ibfk_2` (`phar_id`),
  CONSTRAINT `contains_ibfk_1` FOREIGN KEY (`med_code`) REFERENCES `medicine` (`code`),
  CONSTRAINT `contains_ibfk_2` FOREIGN KEY (`phar_id`) REFERENCES `pharmacy` (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contains`
--

LOCK TABLES `contains` WRITE;
/*!40000 ALTER TABLE `contains` DISABLE KEYS */;
INSERT INTO `contains` VALUES (1,1),(2,1),(3,1),(4,1),(20,1),(7,2),(8,2),(15,2),(16,2),(17,2),(18,2),(19,2),(4,3),(9,3),(10,3),(11,3),(12,3),(13,3),(17,3);
/*!40000 ALTER TABLE `contains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `janitor`
--

DROP TABLE IF EXISTS `janitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `janitor` (
  `janitor_id` int NOT NULL,
  `cleansroom` int NOT NULL,
  PRIMARY KEY (`janitor_id`,`cleansroom`),
  KEY `janitor_ibfk_2` (`cleansroom`),
  CONSTRAINT `janitor_ibfk_1` FOREIGN KEY (`janitor_id`) REFERENCES `Employee` (`employee_id`),
  CONSTRAINT `janitor_ibfk_2` FOREIGN KEY (`cleansroom`) REFERENCES `Room` (`Room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `janitor`
--

LOCK TABLES `janitor` WRITE;
/*!40000 ALTER TABLE `janitor` DISABLE KEYS */;
INSERT INTO `janitor` VALUES (18,1),(19,1),(21,1),(19,2),(21,2),(20,3),(21,4),(18,5),(19,6),(20,7);
/*!40000 ALTER TABLE `janitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicine`
--

DROP TABLE IF EXISTS `medicine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medicine` (
  `code` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `company` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine`
--

LOCK TABLES `medicine` WRITE;
/*!40000 ALTER TABLE `medicine` DISABLE KEYS */;
INSERT INTO `medicine` VALUES (1,'Hydroxyurea Capsules','Cipla'),(2,'Enzalutamide Bdenza','BDR'),(3,'Fenugreek Capsules','Ayurleaf Herbals'),(4,'Hepcvir L tablets','Cipla'),(5,'Itraconazole capsule','Salveo lifecare'),(6,'Biofin 250','Schwitz biotech'),(7,'Lomustine capsules','Mediclone'),(8,'Evarolimus tablets','Novartis'),(9,'Sectral','Sanofi Aventis'),(10,'Olmi fab 20','Salveo lifecare'),(11,'Arthritispainrelief','VXL'),(12,'Solicept','LUPIN ltd'),(13,'Conbinil','Radius Healthcare'),(14,'minipress 2mg','Pfizer'),(15,'AbacavirLamivudine','Hetero Healthcare'),(16,'Azacitidine','Natco'),(17,'Ajithlay MR','Radico remedies'),(18,'Panidop D','SGAS'),(19,'Foracort forte','Cipla'),(20,'BDenza 40 mg','BDR Pharmaceutical');
/*!40000 ALTER TABLE `medicine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pharmacy`
--

DROP TABLE IF EXISTS `pharmacy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pharmacy` (
  `shop_id` int NOT NULL AUTO_INCREMENT,
  `located_in_floor` int NOT NULL,
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pharmacy`
--

LOCK TABLES `pharmacy` WRITE;
/*!40000 ALTER TABLE `pharmacy` DISABLE KEYS */;
INSERT INTO `pharmacy` VALUES (1,1),(2,2),(3,3);
/*!40000 ALTER TABLE `pharmacy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prescription`
--

DROP TABLE IF EXISTS `prescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prescription` (
  `patient_id` int NOT NULL,
  `date_time` timestamp NOT NULL,
  `medicine_code` int NOT NULL,
  PRIMARY KEY (`patient_id`,`date_time`,`medicine_code`),
  KEY `prescription_ibfk_2` (`date_time`),
  KEY `prescription_ibfk_3` (`medicine_code`),
  CONSTRAINT `prescription_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Checkup` (`patient_id`),
  CONSTRAINT `prescription_ibfk_2` FOREIGN KEY (`date_time`) REFERENCES `Checkup` (`datetime`),
  CONSTRAINT `prescription_ibfk_3` FOREIGN KEY (`medicine_code`) REFERENCES `medicine` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prescription`
--

LOCK TABLES `prescription` WRITE;
/*!40000 ALTER TABLE `prescription` DISABLE KEYS */;
INSERT INTO `prescription` VALUES (5,'2019-09-13 11:05:00',7),(1,'2019-09-18 10:00:00',1),(2,'2019-09-18 10:00:00',5),(3,'2019-09-18 10:00:00',2),(4,'2019-09-18 11:05:00',5);
/*!40000 ALTER TABLE `prescription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prescription_operation`
--

DROP TABLE IF EXISTS `prescription_operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prescription_operation` (
  `patient_id` int NOT NULL,
  `admitted_time` timestamp NOT NULL,
  `medicine_code` int NOT NULL,
  PRIMARY KEY (`patient_id`,`admitted_time`,`medicine_code`),
  KEY `prescription_operation_ibfk_2` (`admitted_time`),
  KEY `prescription_operation_ibfk_3` (`medicine_code`),
  CONSTRAINT `prescription_operation_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `Operation` (`patient_id`),
  CONSTRAINT `prescription_operation_ibfk_2` FOREIGN KEY (`admitted_time`) REFERENCES `Operation` (`admitted_time`),
  CONSTRAINT `prescription_operation_ibfk_3` FOREIGN KEY (`medicine_code`) REFERENCES `medicine` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prescription_operation`
--

LOCK TABLES `prescription_operation` WRITE;
/*!40000 ALTER TABLE `prescription_operation` DISABLE KEYS */;
INSERT INTO `prescription_operation` VALUES (6,'2019-09-17 10:00:00',9),(6,'2019-09-17 10:00:00',10),(7,'2019-09-17 10:00:00',2),(7,'2019-09-17 10:00:00',3),(8,'2019-09-17 10:00:00',6),(8,'2019-09-17 10:00:00',9);
/*!40000 ALTER TABLE `prescription_operation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_codes`
--

DROP TABLE IF EXISTS `state_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `state_codes` (
  `pincode` int NOT NULL,
  `state` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`pincode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_codes`
--

LOCK TABLES `state_codes` WRITE;
/*!40000 ALTER TABLE `state_codes` DISABLE KEYS */;
INSERT INTO `state_codes` VALUES (1420,'Massachusetts'),(6611,'Connecticut'),(11050,'New York'),(12804,'New York'),(14701,'New York'),(20901,'Maryland'),(23703,'Virginia'),(27529,'North Carolina'),(28540,'North Carolina'),(28803,'North Carolina'),(29150,'South Carolina'),(29841,'South Carolina'),(33414,'Florida'),(36605,'Alabama'),(44512,'Ohio'),(46140,'Indiana'),(46342,'Indiana'),(47711,'Indiana'),(68107,'Nebraska'),(77904,'Texas'),(80003,'Colorado'),(95677,'California'),(95820,'California');
/*!40000 ALTER TABLE `state_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `works_in`
--

DROP TABLE IF EXISTS `works_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `works_in` (
  `dept_name` varchar(20) NOT NULL,
  `employee_id` int NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `works_in_ibfk_1` (`dept_name`),
  CONSTRAINT `works_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `Employee` (`employee_id`),
  CONSTRAINT `works_in_ibfk_1` FOREIGN KEY (`dept_name`) REFERENCES `Department` (`deptname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `works_in`
--

LOCK TABLES `works_in` WRITE;
/*!40000 ALTER TABLE `works_in` DISABLE KEYS */;
INSERT INTO `works_in` VALUES ('Anesthesiology',10),('Anesthesiology',12),('Anesthesiology',13),('Cardiology',2),('Cardiology',3),('Neurology',5),('Neurology',7),('Oncology',6),('Oncology',8),('Pediatrics',1),('Pediatrics',4),('Pediatrics',11),('Pediatrics',14);
/*!40000 ALTER TABLE `works_in` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-08 21:22:27
