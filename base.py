# starting the loop
import subprocess as sp
import pymysql,pymysql.cursors
import re

def function1():
    print("Patients:")
    query="SELECT * FROM Patient"
    cur.execute(query)
    output = cur.fetchall()
    for eachr in output:
        print("Name:",eachr['Fname'],eachr['Lname'],"patient id:",eachr['patient_id'],"gender:",eachr['sex'],"Phone number: +",eachr['country_code'],eachr['phone_number'],"DOB:",eachr['DOB'],'street:',eachr['street'],"pincode:",eachr['pincode'])
    print("There are a total of {0} patients enrolled".format(len(output)))

def function5():
    string = input("enter the string to search: ")
    query="select * from Employee where (Fname LIKE '"+string+"%' OR Lname LIKE '"+string+"%')  "
    cur.execute(query)
    output = cur.fetchall()
    for eachr in output:
        print("Name :",eachr['Fname'],eachr['Lname'],"phone number : +",eachr['country_code'],eachr['phone_number'])
    print("There are a total of {0} patients enrolled".format(len(output)))


def function2():
    print("Employees whose salary is greater than 50K:")
    query="SELECT * FROM Employee WHERE salary > 50000"
    cur.execute(query)
    output = cur.fetchall()
    for eachr in output:
        print("Name:",eachr['Fname'],eachr['Lname'],"employee id:",eachr['employee_id'],"job type",eachr['job_type'],"gender:",eachr['sex'],"Phone number: +",eachr['country_code'],eachr['phone_number'],"DOB:",eachr['DOB'],"street:",eachr['street'],"pincode:",eachr['pincode'],"present status",eachr['present_status'])
    print("There are a total of {0} Employees with salary greater than 50k".format(len(output)))





def function3():
    print("type the names of professions in a string you want see. The available professions are doctor, druggist, nurse, janitor\n(get the list of all workers of that profession)")
    x=input("type here: ")
    if re.search("[Dd]octor",x):
        print("here are all the Doctors")
        #query="SELECT Employee.*,city_codes.city FROM Employee,Doctor,city_codes,pincode WHERE Doctor.doctor_id=Employee.employee_id"
        query = "SELECT Fname,Lname FROM Employee where job_type='Doctor'"
        cur.execute(query)
        output = cur.fetchall()
        for eachr in output:
            print("Name:",eachr['Fname'],eachr['Lname'],)
        print("total {0} doctors worked in the hospital till now".format(len(output)))

    if re.search("[Nn]urse",x):
        print("here are all the Nurses")
        query="SELECT Fname,Lname FROM Employee where job_type='Nurse'"
        cur.execute(query)
        output = cur.fetchall()
        for eachr in output:
            print("Name:",eachr['Fname'],eachr['Lname'])
        print("total {0} Nurses worked in the hospital till now".format(len(output)))

    if re.search("[Dd]ruggist",x):
        print("here are all the Druggists")
        query="SELECT Fname,Lname FROM Employee where job_type='Druggist'"
        cur.execute(query)
        output = cur.fetchall()
        for eachr in output:
            print("Name:",eachr['Fname'],eachr['Lname'])
        print("total {0} Druggists worked in the hospital till now".format(len(output)))    

    if re.search("[Jj]anitor",x):
        print("here are all the Janitors")  
        query = "SELECT Fname,Lname FROM Employee where job_type='Janitor'"
        cur.execute(query)
        output = cur.fetchall()
        for eachr in output:
            print("Name:",eachr['Fname'],eachr['Lname'])
        print("total {0} Janitors worked in the hospital till now".format(len(output)))

def function6():
    print("Email Id of doctors living in the area with pincode 27529")
    query="SELECT D.email_id AS EMAIL,E.Fname as Fname,E.Lname AS Lname FROM Employee E,Doctor D WHERE E.employee_id = D.doctor_id AND E.pincode=27529"
    cur.execute(query)
    output = cur.fetchall()
    for eachr in output:
        print("Name:",eachr['Fname'],eachr['Lname'],"Email Id:",eachr['EMAIL'])

def function4():
    query="SELECT MAX(salary) AS MAX FROM Employee"
    cur.execute(query)
    output = cur.fetchall()
    for eachr in output:
        print("Maximum salary:",eachr['MAX'])

def function7():
    print("choose option 1 for all departments")
    print("choose option 2 for some departments. give the ' ' seperated department names")
    query= "SELECT deptname AS dm FROM Department"
    cur.execute(query)
    depts=cur.fetchall()
    ian=int(input("option: "))
    if ian==1:
        print("all the employees in their respective departments")
        for ev in depts:
            print("DEPT NAME:",ev['dm'])
            query="SELECT em.Fname as fn,em.Lname as ln,em.job_type AS jt FROM Employee em JOIN works_in wi ON em.employee_id=wi.employee_id WHERE wi.dept_name='{0}'".format(ev['dm'])
            cur.execute(query)
            outi=cur.fetchall()
            for ppr in outi:
                print("Name:",ppr['fn'], ppr['ln'],"works as :",ppr['jt'])
                print("")
    elif ian==2:
        print("Available Departments:")
        for ev in depts:
            print("DEPT NAME:",ev['dm'])
        nl=input("give the dept names: ").split()
        for i in range(len(nl)):
            nl[i]=nl[i].rstrip().lstrip().capitalize()
        for ev in nl:
            print("DEPT NAME:",ev)
            query="SELECT em.Fname as fn,em.Lname as ln,em.job_type AS jt FROM Employee em JOIN works_in wi ON em.employee_id=wi.employee_id WHERE wi.dept_name='{0}'".format(ev)
            cur.execute(query)
            outi=cur.fetchall()
            if len(outi)>0:
                for ppr in outi:
                    print("Name:",ppr['fn'], ppr['ln'],'works as a:',ppr['jt'])
            else:
                print("no such department")
            print("")
    else:
        print("no such option")




def function9():
    print("updating salary")
    try:
        eid=int(input("please give the employee id of the person you want to update"))
        news = int(input("please give the new salary"))
        query = "UPDATE Employee SET salary={0} WHERE employee_id={1}".format(
            news , eid
        )
        rtv=cur.execute(query)  # returns the total no.of rows updated
        con.commit()
        print("total {} rows are affected ".format(rtv))
    except:
        con.rollback()
        print("insert failed due to",e)    

def function10():    
    print("updating address")
    try:
        eid=int(input("please give the employee id of the person you want to update"))
        sali = input("please give the new street name")
        pini = int(input("give the new pincode"))
        citi = input("enter the city name")
        query = "UPDATE Employee SET street='{0}',pincode={1} WHERE employee_id={2}".format(
            sali, pini, eid
        )
        rtv=cur.execute(query)  # returns the total no.of rows updated
        con.commit()
        # query = "SELECT pincode FROM city_codes WHERE pincode={0}".format(citi)
        # rtv1 = cur.execute(query)
        # output=cur.fetchall()
        # if len(output)==0:
        #     query1="INSERT INTO city_codes VALUES({0},'{1}')".format(pini,citi)
        #     cur.execute(query1)
        #     con.commit()
        # if pini in output:
        #     print("total {} rows are affected ".format(rtv))
        print("total {} rows are affected ".format(rtv))
    except Exception as e:
        con.rollback()
        print("insert failed due to",e)



def function8():
    try:
        newr=dict()
        name=input("Name->(Fname Lname) : ").split()
        newr['Fname'] = name[0]
        newr['Lname'] = name[1]
        newr['country_code']=int(input("Enter the country code : "))
        dobi = input("YYYY-MM-DD : ")
        newr['dob']=dobi
        si = input('gender please : ')
        if not re.search("^[MFOmfo]$",si):
            raise genderinputerror
        newr['sex']=si
        newr['street'] = input('street : ')
        newr['pincode'] = int(input('pincode : '))
        # citi = input('city name please ')
        phni=input("Phone number : ")
        if not re.search("^[0-9]{9}$",phni):
            raise phonenumbererror
        newr['phone_number'] = phni
        query="INSERT INTO Patient(Fname,Lname,country_code,DOB,sex,pincode,phone_number,street) VALUES('{0}', '{1}', {2},'{3}', '{4}', {5}, {6},'{7}')".format(
            newr['Fname'],newr['Lname'],newr['country_code'],newr['dob'],newr['sex'],newr['pincode'],newr['phone_number'],newr['street']
        )
        rtv=cur.execute(query)
        con.commit()
        # query1 = "SELECT pincode FROM city_codes WHERE pincode='{0}'".format(citi)
        # rtv1 = cur.execute(query1)
        # output=cur.fetchall()
        # if len(output)==0:
        #     query1="INSERT INTO city_codes VALUES({0},'{1}')".format(newr['pincode '],citi)
        #     cur.execute(query1)
        #     con.commit()
        print("total {} rows are affected".format(rtv))
    except Exception as e:
        con.rollback()
        print("insert failed due to",e)


def function13():
    print("for the profession of the new employee")
    print("Enter 1 for doctor\n2 for nurse\n3 for druggist\n4 for janitor\n")
    prof= int(input())
    try:
        emid = -2
        newr=dict()
        name=input("Name->(Fname Lname) ").split()
        newr['Fname'] = name[0]
        newr['Lname'] = name[1]
        newr['country_code']=int(input("country_code: "))
        dobi = input("YYYY-MM-DD : ")
        newr['dob']=dobi
        si = input('gender please ')
        if not re.search("^[MFOmfo]$",si):
            raise genderinputerror
        newr['sex']=si
        addri=input('street')
        newr['street'] = addri
        citi=input("enter the city name :")
        newr['pincode'] = int(input("pincode: "))
        phni=input("Phone number: ")
        if not re.search("[0-9]{9}$",phni):
            raise phonenumbererror
        newr['phone_number'] = int(phni)
        newr['job_type']=input("job type (doctor,nurse,druggist,janitor): ")
        newr['present_Status'] = 'Currently working in this hospital.'
        newr['salary']= int(input("give the salary"))
        query="INSERT INTO Employee(Fname,Lname,country_code,DOB,sex,pincode,phone_number,salary,job_type,present_status,street) VALUES('{0}', '{1}', {2}, '{3}', '{4}', {5}, {6}, {7},'{8}','{9}','{10}')".format(
            newr['Fname'],newr['Lname'],newr['country_code'],newr['dob'],newr['sex'],newr['pincode'],newr['phone_number'],newr['salary'],newr['job_type'],newr['present_Status'],newr['street']
        )
        rtv=cur.execute(query)
        con.commit()
        print("total {} rows affected".format(rtv), "inserted into database")
        query="SELECT employee_id FROM Employee WHERE Fname='{0}' AND Lname='{1}' AND street='{2}' AND DOB='{3}'".format(
            name[0],name[1],addri,dobi
        )
        cur.execute(query)
        output=cur.fetchall()
        if len(output)>0:
            emid=output[0]['employee_id']
            print(emid)
        else:
            raise ConnectionError
        # query1 = "SELECT pincode FROM city_codes WHERE pincode='{0}'".format(citi)
        # rtv1 = cur.execute(query1)
        # output=cur.fetchall()
        # if len(output)==0:
        #     query1="INSERT INTO city_codes VALUES({0},'{1}')".format(newr['pincode '],citi)
        #     cur.execute(query1)
        #     con.commit()
        if prof==1:
            print("lets fill data about his trainer")
            edid=input("doctor's email-id ")
            hid= int(input("his trainer's doctor_id"))
            query="INSERT INTO Doctor(doctor_id,trainer_id,email_id) VALUES({0},{1},'{2}')".format(emid,hid,edid)
            cur.execute(query)
            con.commit()
            print("inserted into database")
            print("lets update the doctor's medical background ")
            prac_info=input("please give practice info ")
            deg_info=input("info about his degree ")
            ed_info=input("educational background ")
            query="INSERT INTO Doctor_Medical_Background(doctor_id,practice_info,degree,education_background) VALUES({0},'{1}','{2}','{3}')".format(
                emid,prac_info,deg_info,ed_info
            )
            cur.execute(query)
            con.commit()
            print("inserted into database")
        if prof==2:
            print("lets fill data about his trainer")
            edid=input("nurse's email-id ")
            hid= int(input("his trainers doctor_id"))
            query="INSERT INTO Nurse(nurse_id,trainer_id,email_id) VALUES({0},{1},'{2}')".format(emid,hid,edid)
            cur.execute(query)
            con.commit()
            print("inserted into database")
            print("lets add the nurse's medical background ")
            prac_info=input("please give practice info ")
            deg_info=input("info about his/her degree ")
            ed_info=input("educational background ")
            query="INSERT INTO Nurse_Medical_Background(nurse_id,practice_info,degree,education_background) VALUES({0},'{1}','{2}','{3}')".format(
                emid,prac_info,deg_info,ed_info
            )
            cur.execute(query)
            con.commit()
            print("inserted into database")
        if prof==3:
            print("inserted into database")
            print("lets fill data like where he works")
            edid=input("nurse's email-id ")
            hid= int(input("shop id in which he works."))
            query="INSERT INTO Druggist(druggist_id,working_shop_id,email_id) VALUES({0},{1},'{2}')".format(emid,hid,edid)
            cur.execute(query)
            con.commit()
            print("inserted into database")
            print("lets update the druggist's medical background ")
            prac_info=input("please give practice info ")
            deg_info=input("info about his/her degree ")
            ed_info=input("educational background ")
            query="INSERT INTO Druggist_Medical_Background(druggist_id,practice_info,degree,education_background) VALUES({0},'{1}','{2}','{3}')".format(
                emid,prac_info,deg_info,ed_info
            )
            cur.execute(query)
            con.commit()
            print("inserted into database")
        if prof==4:
            print("lets fill data like where he/she works")
            rid=int(input("room id in which he/she cleans: "))
            query="INSERT INTO janitor(janitor_id,cleansroom) values({0},{1})".format(emid,rid)
            cur.execute(query)
            con.commit()
            print("inserted into database.")
    except Exception as e:
        con.rollback()
        print("insert failed due to",e)




def function11():
    try:
        x=int(input("id of employee of which you are going to change status "))
        print("give 1 if a current employee is leaving 2 if ex-employee returns")
        y=int(input("option"))
        if y==1:
            reason=input('what next is he going to do')  # like working
            query = "UPDATE Employee SET present_status='{0}',salary=NULL WHERE employee_id={1}".format(reason,x)
            cur.execute(query)
            con.commit()
            print("updated into database")
        elif y==2:
            sali = int(input("salary: "))
            query = "UPDATE Employee SET present_status='Currently working in this hospital.',salary={0} WHERE employee_id={1}".format(sali,x)
            cur.execute(query)
            con.commit()
            print("updated into database")
    except Exception as e:
        con.rollback()
        print("insert failed due to",e)

def function12():
    try:
        pid=int(input("please give the patient id to delete his prescriptions and stuff : "))
        query="DELETE FROM prescription_operation WHERE patient_id={0}".format(pid)
        cur.execute(query)
        con.commit()
        query="DELETE FROM prescription WHERE patient_id={0}".format(pid)
        cur.execute(query)
        con.commit()
        print("deleted data related to him/her in the tables prescription and prescription_operation")
        # query="DELETE FROM Checkup WHERE patient_id={0}".format(pid)
        # cur.execute(query)
        # con.commit()
        # query="DELETE FROM Operation WHERE patient_id={0}".format(pid)
        # cur.execute(query)
        # con.commit()
        # query="DELETE FROM Patient WHERE patient_id={0}".format(pid)
        # cur.execute(query)
        # con.commit()
    except Exception as e:
        print("failed due to error",e)
       

def dispatch(n):
    if n==1:
        function1()
    elif n==2:
        function2()
    elif n==3:
        function3()
    elif n==4:
        function4()
    elif n==5:
        function5()
    elif n==6:
        function6()
    elif n==7:
        function7()
    elif n==8:
        function8()
    elif n==9:
        function9()
    elif n==10:
        function10()
    elif n==11:
        function11()    
    elif n==12:
        function12()
    elif n==13:
        function13()
    else:
        print("invalid option")


chk=1
while chk:
    tmp = sp.call('clear', shell=True)
    try:
        con = pymysql.connect(host='127.0.0.1',user=input("username:"),password=input("password :"),db='HOSPITAL',port=5005,cursorclass=pymysql.cursors.DictCursor)
        tmp = sp.call('clear', shell=True)
        if(con.open):
            print("Connected")
        else:
            print("Failed to connect")
        tmp = input("Enter any key to CONTINUE>")
        with con.cursor() as cur:
            while(1):
                tmp = sp.call('clear', shell=True)
                print("1. Display the entire data tuples of Patients who got enrolled.")
                print("2. Display the entire data tuples of Employees whose salary is greater than 50,000.")                      
                print("3. Get the names of workers of some profession") 
                print("4. Maximum salary, out of the employees that are working now.")                       
                print("5. Display name,ssn of the eemployees whose phone number starts with a particular string.")
                print("6. Email Id of doctors who live in a locality with pincode 27529")    
                print("7. Display name, job type of employees working in the mentioned department ")
                print("8. Enroll a new patient")                
                print("9. Update salary of an employee")
                print("10. Update address of an employee")
                print("11. Update current_status of employee")
                print("12. Delete all the tuples related to `medicines consumed` by a particular patient.")
                print("13. Add a new employee")
                print("14. close")
                ch = int(input("Enter choice> "))
                tmp = sp.call('clear', shell=True)
                if ch == 14:
                    con.close()
                    chk=0
                    break
                else:
                    dispatch(ch)
                    tmp = input("Enter any key to CONTINUE>")            
    except:
        tmp = sp.call('clear', shell=True)
        print("Connection Refused: Either username or password is incorrect or user doesn't have access to database")
        tmp = input("Enter any key to CONTINUE>")
